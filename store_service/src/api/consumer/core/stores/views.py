from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework import mixins

from stores.models import Store
from .serializers import StoreSerializer


class StoreViewSet(
            mixins.CreateModelMixin,
            mixins.ListModelMixin,
            mixins.RetrieveModelMixin,
            viewsets.GenericViewSet
        ):
    serializer_class = StoreSerializer
    queryset = Store.objects.all()

    def get_filter_kwargs(self):
        filter_kwargs = {}
        # Process request query args filters here
        return filter_kwargs

    def get_queryset(self):
        filter_kwargs = self.get_filter_kwargs()

        # User filter for doc listing permission
        if self.request.user:
            filter_kwargs.update({'associated_to': self.request.user.id})
            return Store.objects.filter(**filter_kwargs)
        else:
            # return Store.objects.none()
            return Store.objects.all()   # temporary. no auth on endpoint

    def post(self, request):
        # todo: store create view handling
        serializer = StoreSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
