from rest_framework import serializers

from stores.models import Store


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = '__all__'

    def validate(self, attrs):
        return super(StoreSerializer, self).validate(attrs)

    def create(self, validated_data):
        return super(StoreSerializer, self).create(validated_data)
