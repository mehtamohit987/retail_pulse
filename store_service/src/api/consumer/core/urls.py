from django.conf.urls import include, url


urlpatterns = [
    url(r'^stores/', include('api.consumer.core.stores.urls')),
    url(r'^visits/', include('api.consumer.core.visits.urls')),
]
