from rest_framework import serializers

from visits.models import Visit


class VisitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visit
        fields = '__all__'

    def validate(self, attrs):
        return super(VisitSerializer, self).validate(attrs)

    def create(self, validated_data):
        return super(VisitSerializer, self).create(validated_data)
