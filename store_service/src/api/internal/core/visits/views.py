from rest_framework import viewsets, mixins
from rest_framework import permissions

from visits.models import Visit
from .serializers import VisitSerializer


class VisitViewSet(
            mixins.CreateModelMixin,
            mixins.UpdateModelMixin,
            mixins.RetrieveModelMixin,
            viewsets.GenericViewSet
        ):
    queryset = Visit.objects.all()
    serializer_class = VisitSerializer
    permission_classes = [permissions.IsAdminUser]

    def perform_create(self, serializer):
        # todo: nested write - seller fk associate/create, blob data mangling
        serializer.save(owner=self.request.user)
