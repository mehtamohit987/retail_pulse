from django.conf.urls import include, url


urlpatterns = [
    url(r'^stores/', include('api.internal.core.stores.urls')),
    url(r'^visits/', include('api.internal.core.visits.urls')),
]
