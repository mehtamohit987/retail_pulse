from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext as _

from visits.models import Visit


class StoreStatusEnum(models.IntegerChoices):
    PENDING = 0, _('Pending')
    UNDER_PROCESS = 1, _('Under Process')
    DIGITIZED = 2, _('Digitized')
    RE_REQUESTED = 3, _('Re Requested')
    ADDITIONAL_DOC_REQ = 4, _('Additional Store Required')

    __empty__ = _('(Unknown)')


class Store(models.Model):
    associated_to = models.ForeignKey(User, on_delete=models.SET_NULL, blank=False, null=True, )
    file_resource_uri = models.URLField(max_length=400)
    associated_visit = models.ForeignKey(Visit, on_delete=models.SET_NULL, blank=True, null=True, )
    status = models.IntegerField(choices=StoreStatusEnum.choices, default=StoreStatusEnum.PENDING,
                                 blank=False, null=False)
    source_store = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created_at']
