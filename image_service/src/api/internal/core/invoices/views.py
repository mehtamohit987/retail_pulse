from rest_framework import viewsets, mixins
from rest_framework import permissions

from images.models import Image
from .serializers import ImageSerializer


class ImageViewSet(
            mixins.CreateModelMixin,
            mixins.UpdateModelMixin,
            mixins.RetrieveModelMixin,
            viewsets.GenericViewSet
        ):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = [permissions.IsAdminUser]

    def perform_create(self, serializer):
        # todo: nested write - seller fk associate/create, blob data mangling
        serializer.save(owner=self.request.user)
