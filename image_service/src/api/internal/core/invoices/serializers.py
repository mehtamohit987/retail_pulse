from rest_framework import serializers

from images.models import Image


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'

    def validate(self, attrs):
        return super(ImageSerializer, self).validate(attrs)

    def create(self, validated_data):
        return super(ImageSerializer, self).create(validated_data)
