from rest_framework import viewsets
from rest_framework import mixins

from jobs.models import Job
from .serializers import JobSerializer


class JobViewSet(
            mixins.UpdateModelMixin,
            mixins.ListModelMixin,
            mixins.RetrieveModelMixin,
            viewsets.GenericViewSet
        ):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
