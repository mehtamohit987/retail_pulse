from django.conf.urls import include, url


urlpatterns = [
    url(r'^jobs/', include('api.internal.core.jobs.urls')),
    url(r'^images/', include('api.internal.core.images.urls')),
]
