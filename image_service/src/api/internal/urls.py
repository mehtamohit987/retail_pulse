from django.conf.urls import include, url


urlpatterns = [
    url(r'^core/', include('api.internal.core.urls')),
]
