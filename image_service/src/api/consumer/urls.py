from django.conf.urls import include, url


urlpatterns = [
    url(r'^v1/', include('api.consumer.v1.urls')),
    url(r'^core/', include('api.consumer.core.urls')),
]
