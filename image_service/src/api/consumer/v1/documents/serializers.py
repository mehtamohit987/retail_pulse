from rest_framework import serializers

from jobs.models import Job


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'

    def validate(self, attrs):
        return super(JobSerializer, self).validate(attrs)

    def create(self, validated_data):
        return super(JobSerializer, self).create(validated_data)
