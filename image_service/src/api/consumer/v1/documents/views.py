from django.http import HttpResponse, JsonResponse

from jobs.models import Job


def job_status(request, pk):
    if request.method == 'GET':
        try:
            doc_status = Job.objects.values_list('status', flat=True).get(pk=pk)
        except Job.DoesNotExist:
            return HttpResponse(status=404)

        # todo: user associated
        #     serializer = JobSerializer(job)
        #     return JsonResponse(serializer.data)
        return JsonResponse({'status': doc_status})
    return HttpResponse(status=404)
