from django.conf.urls import include, url


urlpatterns = [
    url(r'^jobs/', include('api.consumer.v1.jobs.urls')),
]
