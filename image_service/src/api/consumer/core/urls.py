from django.conf.urls import include, url


urlpatterns = [
    url(r'^jobs/', include('api.consumer.core.jobs.urls')),
    url(r'^images/', include('api.consumer.core.images.urls')),
]
