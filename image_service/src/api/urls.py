from django.conf.urls import include, url


urlpatterns = [
    url(r'^internal/', include('api.internal.urls')),
    url(r'^consumer/', include('api.consumer.urls')),
]
