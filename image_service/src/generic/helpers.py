
def clean_non_ascii(value):
    return ''.join([i if ord(i) < 128 else '' for i in value])
