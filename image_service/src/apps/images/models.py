from django.contrib.auth.models import User
from django.db import models


class ExchangeParty(models.Model):
    name = models.CharField(max_length=50)
    address = models.TextField()
    mapped_user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, )


class Image(models.Model):
    associated_to = models.ForeignKey(User, on_delete=models.SET_NULL, blank=False, null=True, )
    number = models.CharField(max_length=30, blank=False, null=False)
    date = models.DateField(blank=False, null=False)
    seller = models.ForeignKey(ExchangeParty, on_delete=models.RESTRICT, blank=False, null=False)
    buyer = models.ForeignKey(ExchangeParty, on_delete=models.RESTRICT, related_name='buyer', blank=True, null=True)
    order = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['updated_at']
